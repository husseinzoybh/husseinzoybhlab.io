import React from 'react'
import './portfolio.css'
import IMG1 from '../../assets/portfolio1.png'
import IMG2 from '../../assets/portfolio2.png'
import IMG3 from '../../assets/portfolio3.png'
import IMG4 from '../../assets/portfolio4.png'
import IMG5 from '../../assets/portfolio5.png'
import IMG6 from '../../assets/portfolio6.png'

const data = [
    {
        id: 1,
        image: IMG1,
        title: "Autobahn",
        gitlab: 'https://gitlab.com/jwpettit/project-beta'

    },
    {
        id: 2,
        image: IMG3,
        title: "Django Accounts App",
        gitlab: 'https://gitlab.com/husseinzoybh/django-two-shot'

    },
    {
        id: 3,
        image: IMG2,
        title: "Conference App",
        gitlab: 'https://gitlab.com/husseinzoybh/fearless-frontend'

    },
    {
        id: 4,
        image: IMG4,
        title: "Jam-Pack'd",
        gitlab: 'https://gitlab.com/semi-serious-solutions/jam-packd'

    },
    {
        id: 5,
        image: IMG5,
        title: "Task Manager App",
        gitlab: 'https://gitlab.com/husseinzoybh/project-alpha-apr'

    },
    {
        id: 6,
        image: IMG6,
        title: "React Portfolio Website",
        gitlab: 'https://gitlab.com/husseinzoybh/husseinzoybh.gitlab.io'

    },
]



const Portfolio = () => {
    return (
        <section id='portfolio'>
            <h5> My Recent Work</h5>
            <h2>Portfolio</h2>

            <div className='container portfolio__container'>
                {
                    data.map(({ id, image, title, gitlab }) => {
                        return (
                            <article key={id} className='portfolio__item'>
                                <div className='portfolio__item-image'>
                                    <img src={image} alt='title' />
                                </div>
                                <h3>{title}</h3>
                                <div className='portfolio__item-cta'>
                                    <a href={gitlab} className='btn' target='_blank' rel="noreferrer" >Gitlab</a>
                                </div>
                            </article>
                        )

                    })
                }


            </div>

        </section>
    )
}

export default Portfolio