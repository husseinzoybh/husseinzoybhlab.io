import React from 'react'
import './testimonials.css'
import avtr1 from '../../assets/avtr1.png'
import avtr2 from '../../assets/avtr2.png'
import avtr3 from '../../assets/avtr3.png'
import avtr4 from '../../assets/avtr4.png'

// import Swiper core and required modules
import { Pagination } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';



const data = [
    {
        avatar: avtr1,
        reviewer: 'Lee Pham',
        review: 'Zoybh is an incredibly knowledgeable, hard-working individual. He is always willing to pitch in and help, and truly cares about group success. He works with great efficiency and effectiveness. It was a pleasure working with him and I hope to have the opportunity to work together again in the future.'
    },
    {
        avatar: avtr2,
        reviewer: 'Adrian Dorado',
        review: ' I Highly recommend Zoybh Hussein for any software engineering position. As a graduate of the highly reputable Hack Reactor bootcamp, Zoybh has received top-notch training in a variety of programming languages and technologies, including HTML, CSS, React, JavaScript, Django, and Python. Zoybh has consistently demonstrated a strong aptitude for software development, as well as a deep understanding of best practices in the field. They are proficient in continuous integration and deployment (CI/CD) and unit testing, and have consistently demonstrated their ability to work well with teammates in the face of adversity. Zoybh is also a highly collaborative and team-oriented individual. They have a strong work ethic and are always willing to go above and beyond to ensure the success of a project. I have no doubt that Zoybh would be a valuable addition to any development team, and I highly recommend them for any software engineering position.'
    },
    {
        avatar: avtr3,
        reviewer: 'Tyler Dempsey',
        review: 'I had the pleasure of working with Zoybh Hussein for the past 19 weeks and was consistently impressed by his dedication, intelligence, and work ethic In his role as software engineering student, Zoybh consistently demonstrated his ability to handle complex tasks and projects with a high level of efficiency and attention to detail. He is a quick learner and is always eager to take on new challenges. In addition, he has excellent communication and interpersonal skills, making him a valuable asset to any team. I highly recommend Zoybh for any opportunity in the software field. He would be an asset to any company and I believe he will continue to excel in his career.'
    },
    {
        avatar: avtr4,
        reviewer: 'John Gardner',
        review: 'Zoybh is an outstanding software engineer. He has a strong technical foundation and is always eager to learn and improve his skills. He is a team player and is always willing to lend a helping hand to his colleagues. In the time that I have worked with Zoybh, he has consistently impressed me with his dedication to delivering high-quality work. He is a reliable and efficient problem-solver, and his attention to detail is second to none. I highly recommend Zoybh as a software engineer. He would be an asset to any team and I am confident that he will continue to excel in his career.'
    }
]

const Testimonials = () => {
    return (
        <section id='testimonials'>
            <h5>My Reviews</h5>
            <h2>Testimonials</h2>
            <Swiper className='container testimonials__container'
                // install Swiper modules
                modules={[Pagination]}
                spaceBetween={40}
                slidesPerView={1}
                pagination={{ clickable: true }}>
                {
                    data.map(({ avatar, reviewer, review }, index) => {
                        return (
                            <SwiperSlide key={index} className='testimonial'>
                                <div className='client__avatar'>
                                    <img src={avatar} alt="" />
                                </div>
                                <h5 className='client__name'>{reviewer}</h5>
                                <small className='client__review'>{review}</small>
                            </SwiperSlide>
                        )

                    })
                }
            </Swiper>

        </section>
    )
}

export default Testimonials