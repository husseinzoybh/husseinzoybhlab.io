import React from 'react'
import './contact.css'
import { MdOutlineEmail } from 'react-icons/md'
import { AiOutlinePhone } from 'react-icons/ai'
import { useRef } from 'react';
import emailjs from 'emailjs-com';

const Contact = () => {
    const form = useRef();
    const sendEmail = (e) => {
        e.preventDefault();

        emailjs.sendForm('service_uyxsctp', 'template_on05747', form.current, 'PTOJj_CM23veoCK4K')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
    };
    return (
        <section id='contact'>
            <h5>Get In Touch</h5>
            <h2>Contact Me</h2>

            <div className='container contact__container'>
                <div className='contact__options'>
                    <article className='contact__option'>
                        <MdOutlineEmail className='contact__option-icon' />
                        <h4> Email</h4>
                        <h5>Husseinzoybh@gmail.com</h5>
                        <a href='mailto:husseinzoybh@gmail.com' target='_blank' rel='noreferrer'>Send A Message</a>
                    </article>
                    <article className='contact__option'>
                        <AiOutlinePhone className='contact__option-icon' />
                        <h4> Text/Call</h4>
                        <h5>+1 (650)867-9031</h5>
                        <a href='tel:1-650-867-9031' target='_blank' rel='noreferrer'>Send A Message</a>
                    </article>
                </div>
                {/* END OF CONTACT OPTIONS*/}
                <form ref={form} onSubmit={sendEmail}>
                    <input type="text" name='name' placeholder='Your Full Name' required />
                    <input type='email' name='email' placeholder='Your Email' required />
                    <textarea name='message' rows='7' placeholder='Your Message' required></textarea>
                    <button type='submit' className='btn btn-primary'>Send Message</button>
                </form>
            </div>
        </section >
    )
}

export default Contact