import React from 'react'
import { AiOutlineLinkedin } from 'react-icons/ai'
import { SiGitlab } from 'react-icons/si'


const HeaderSocials = () => {
    return (
        <div className="header_socials">
            <a href="https://www.linkedin.com/in/zoybhhussein/" target="_blank" rel="noreferrer" ><AiOutlineLinkedin /></a>
            <a href="https://gitlab.com/husseinzoybh" target="_blank" rel="noreferrer" ><SiGitlab /></a>
        </div>
    )
}

export default HeaderSocials