import React from 'react'
import './about.css'
import A from '../../assets/A.png'
import { FaAward } from 'react-icons/fa'
import { GiGraduateCap } from 'react-icons/gi'
import { SiCodeproject } from 'react-icons/si'

const About = () => {
    return (
        <section id='about'>
            <h5>Get To Know</h5>
            <h2>About Me</h2>

            <div className="container about__container">
                <div className='about__me'>
                    <div className='about-image'>
                        <img src={A} alt="a" />
                    </div>

                </div>
                <div className='about__content'>
                    <div className='about__cards'>
                        <article className='about__card'>
                            <FaAward className='about__icon' />
                            <h5>Experience</h5>
                            <small>800+ Hours Coding</small>
                        </article>

                        <article className='about__card'>
                            <GiGraduateCap className='about__icon' />
                            <h5>Bootcamp</h5>
                            <small>Hack Reactor Graduate</small>
                        </article>

                        <article className='about__card'>
                            <SiCodeproject className='about__icon' />
                            <h5>Projects</h5>
                            <small>5+ Fullstack projects completed and in progress</small>
                        </article>
                    </div>
                    <p>
                        I am a self-motivated maker and software engineer / software developer. Fueled by a passion for learning, I engage and create thoughtfully, acting in the present to innovate responsibly with care for the future. I find fulfillment in empowering others to do their best work by helping them remove the obstacles in their way. My favorite tools to use are the Python, and JavaScript languages, as well as the React and Django frameworks.
                    </p>
                    <a href='#contact' className='btn btn primary'>Let's Chat!</a>
                </div>
            </div>
        </section>
    )
}

export default About